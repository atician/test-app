import { beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';

import * as actions from '..';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const baseURL = 'http://localhost:3001';

describe('action creators', function() {
  let store;
  beforeEach(function() {
    store = mockStore();
  });
  afterEach(function() {
    sinon.reset();
    sinon.restore();
  });
  describe('getNotes', function() {
    it('should dispatch notes on success', function() {
      sinon.stub(axios, 'get').resolves({ data: [{ id: 0, content: 'test' }] });
      const expected = [
        { type: actions.GET_NOTE_REQUEST },
        {
          type: actions.GET_NOTE_SUCCESS,
          notes: [{id: 0, content: 'test'}]
        }
      ];
      return store.dispatch(actions.getNotes()).then(() => {
        sinon.assert.calledOnce(axios.get);
        sinon.assert.calledWithExactly(axios.get, baseURL + '/notes');
        expect(store.getActions()).to.eql(expected);
      });
    });
    it('should dispatch failure on error', function() {
      sinon.stub(axios, 'get').rejects();
      const expected = [
        { type: actions.GET_NOTE_REQUEST },
        { type: actions.GET_NOTE_FAILURE }
      ]
      return store.dispatch(actions.getNotes()).then(() => {
        expect(store.getActions()).to.eql(expected);
      });
    });
  });
  describe('createNote', function() {
    it('should dispatch new note on success', function() {
      sinon.stub(axios, 'post').resolves({data: { id: 0, content: 'test' }});
      const expected = [
        { type: actions.POST_NOTE_REQUEST },
        { type: actions.POST_NOTE_SUCCESS, id: 0, content: 'test'}
      ];
      return store.dispatch(actions.createNote('test')).then(() => {
        sinon.assert.calledOnce(axios.post);
        sinon.assert.calledWithExactly(axios.post, baseURL + '/notes', { content: 'test' });
        expect(store.getActions()).to.eql(expected);
      });
    });
    it('should dispatch failure on error', function() {
      sinon.stub(axios, 'post').rejects();
      const expected = [
        { type: actions.POST_NOTE_REQUEST },
        { type: actions.POST_NOTE_FAILURE }
      ];
      return store.dispatch(actions.createNote('test')).then(() => {
        expect(store.getActions()).to.eql(expected);
      });
    });
  });
});
