import React from 'react';
import PropTypes from 'prop-types';

const Note = ({ id, content }) => (
  <div>
    <h3>{id}: {content}</h3>
  </div>
);
Note.propTypes = {
  id: PropTypes.number,
  content: PropTypes.string
};
export default Note;
