import { combineReducers } from 'redux'

import notes from './notes';
import isLoading from './isLoading';

export default combineReducers({
  notes,
  isLoading
});
