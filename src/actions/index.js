import axios from 'axios';

export const GET_NOTE_REQUEST = 'GET_NOTE_REQUEST';
export const GET_NOTE_SUCCESS = 'GET_NOTE_SUCCESS';
export const GET_NOTE_FAILURE = 'GET_NOTE_FAILURE';
export const POST_NOTE_REQUEST = 'POST_NOTE_REQUEST';
export const POST_NOTE_SUCCESS = 'POST_NOTE_SUCCESS';
export const POST_NOTE_FAILURE = 'POST_NOTE_FAILURE';

export const getNotes = () => dispatch => {
  dispatch({
    type: GET_NOTE_REQUEST
  });
  return axios.get('http://localhost:3001/notes')
  .then(
    res => dispatch({
      type: GET_NOTE_SUCCESS,
      notes: res.data
    }),
    err => dispatch({
      type: GET_NOTE_FAILURE
    })
  );
};

export const createNote = (content) => dispatch => {
  dispatch({
    type: POST_NOTE_REQUEST
  });
  return axios.post('http://localhost:3001/notes', {
    content
  })
  .then(
    res => dispatch({
      type: POST_NOTE_SUCCESS,
      ...res.data
    }),
    err => dispatch({
      type: POST_NOTE_FAILURE,
    })
  );
};
