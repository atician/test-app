import { expect } from 'chai';
import { shallowMount } from '../../test/helper';

import Note from '../note';

const renderComponent = props => shallowMount(Note, props);

describe('<Note />', function() {
  it('should render without crashing', function() {
    const component = renderComponent();
    component.unmount();
  });
  it('should render props', function() {
    const props = {
      id: 1,
      content: 'test'
    };
    const component = renderComponent(props);
    const content = component.text();
    expect(content).to.have.string(props.id);
    expect(content).to.have.string(props.content);
  });
});
