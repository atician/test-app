import {
  GET_NOTE_SUCCESS,
  POST_NOTE_SUCCESS
} from '../actions';

const notes = (state = [], action) => {
  switch (action.type) {
    case GET_NOTE_SUCCESS:
      return [
        ...action.notes
      ];
    case POST_NOTE_SUCCESS:
      return [
        ...state,
        {
          id: action.id,
          content: action.content
        }
      ];
    default:
      return state;
  }
};

export default notes;
