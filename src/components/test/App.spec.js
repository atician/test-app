import React from 'react';
import { expect } from 'chai';
import { shallowMount } from '../../test/helper';

import App from '../App';
import AddNote from '../../containers/AddNote';
import NoteList from '../../containers/NoteList';

const renderComponent = props => shallowMount(App, props);

describe('<App />', function() {
  it('should render without crashing', function() {
    const component = renderComponent();
    component.unmount();
  });
  it('should contain <AddNote /> and <NoteList />', function() {
    const component = renderComponent();
    const children = [ <AddNote />, <NoteList /> ];
    expect(component.containsAllMatchingElements(children)).to.equal(true);
  });
});
