import React, { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import { createNote } from '../actions';

export class AddNote extends Component {
  constructor(props) {
    super(props);
    this.inputRef = createRef();
    this.state = {
      value: ''
    };
  }
  handleChange = (e) => {
    const value = e.target.value.trim();
    if (value) {
      this.setState({ value });
    }
  }
  handleClick = () => {
    const { value } = this.state;
    if (value) {
      this.props.dispatch(createNote(value));
      this.setState({ value: '' });
    }
    this.inputRef.current.focus();
  }
  render() {
    return (
      <div>
        <input
          type="text"
          ref={this.inputRef}
          value={this.state.value}
          onChange={this.handleChange}
        />
        <button onClick={this.handleClick}>
          Add
        </button>
      </div>
    );
  }
}

AddNote.propTypes = {
  dispatch: PropTypes.func
};
export default connect()(AddNote);
