import React, { Component } from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

import { getNotes } from '../actions';
import Note from '../components/Note';

export class NoteList extends Component {
  static propTypes = {
    notes: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      content: PropTypes.string
    })),
    dispatch: PropTypes.func
  };
  static defaultProps = {
    notes: []
  };
  componentWillMount() {
    this.props.dispatch(getNotes());
  }

  render() {
    return this.props.notes.map(post =>
      <Note
        key={post.id}
        id={post.id}
        content={post.content}
      />
    );
  }
}

const mapStateToProps = ({ notes }) => ({
  notes
});

export default connect(
  mapStateToProps
)(NoteList);
