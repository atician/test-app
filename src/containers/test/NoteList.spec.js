import { beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';

import { shallowMount } from '../../test/helper';
import { NoteList } from '../NoteList';
import Note from '../../components/Note';
import * as actions from '../../actions';

const renderComponent = props => {
  const defaultProps = {
    dispatch: sinon.stub()
  };
  return {
    component: shallowMount(NoteList, { ...defaultProps, ...props }),
    props: { ...defaultProps, ...props }
  };
};

describe('<NoteList />', function() {
  beforeEach(function() {
    sinon.stub(actions, 'getNotes');
  });
  afterEach(function() {
    sinon.reset();
    sinon.restore();
  });
  it('should render without crashing', function() {
    const { component } = renderComponent();
    component.unmount();
  });
  it('should get notes on render', function() {
    const { props } = renderComponent();
    sinon.assert.calledOnce(props.dispatch);
    sinon.assert.calledOnce(actions.getNotes);
  });
  it('should not render any notes', function() {
    const { component } = renderComponent();
    expect(component.find(Note)).to.be.empty;
  });
  it('should render notes in order', function() {
    const notes = [
      {
        id: 1,
        content: 'note 1'
      },
      {
        id: 2,
        content: 'note 2'
      },
      {
        id: 3,
        content: 'note 3'
      }
    ];
    const { component } = renderComponent({ notes });
    const noteNodes = component.find(Note);
    expect(noteNodes).to.have.lengthOf(3);
    noteNodes.forEach((node, i) => {
      expect(node.prop('id')).to.equal(notes[i].id);
      expect(node.prop('content')).to.equal(notes[i].content);
    });
  });
});
