import { beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';

import {
  WHITE_SPACES,
  enzymeMount,
  simulateOnChange,
  simulateOnClick
} from '../../test/helper';
import { AddNote } from '../AddNote';
import * as actions from '../../actions';

const TEST_VALUE = 'test';

const renderComponent = props => {
  const defaultProps = {
    dispatch: sinon.spy()
  };
  return {
    component: enzymeMount(AddNote, { ...defaultProps, ...props }),
    props: { ...defaultProps, ...props }
  };
};
describe('<AddNote />', function() {
  let component;
  let props;
  beforeEach(() => {
    sinon.stub(actions, 'createNote');
    ({ component, props } = renderComponent());
  });
  afterEach(() => {
    sinon.restore();
    sinon.reset();
  });
  it('should render without crashing', function() {
    component.unmount();
  });
  it('should focus after submit', function() {
    // without note
    simulateOnClick(component);
    expect(document.activeElement).to.eql(component.find('input').getDOMNode());
    // with valid note
    simulateOnChange(component, TEST_VALUE);
    simulateOnClick(component);
    expect(document.activeElement).to.eql(component.find('input').getDOMNode());
  });
  it('should clear input after submit', function() {
    simulateOnChange(component, TEST_VALUE);
    simulateOnClick(component);
    expect(component.find('input').prop('value')).to.have.string('');
  });
  describe('validation', function() {
    it('should create note with valid input value', function() {
      simulateOnChange(component, TEST_VALUE);
      simulateOnClick(component);
      sinon.assert.calledOnce(props.dispatch);
      sinon.assert.calledOnce(actions.createNote);
      sinon.assert.calledWithExactly(actions.createNote, TEST_VALUE);
    });
    it('should not create note because no input value is given', function() {
      simulateOnChange(component);
      simulateOnClick(component);
      sinon.assert.notCalled(props.dispatch);
      sinon.assert.notCalled(actions.createNote);
    });
    it('should not create note because only white space is given', function() {
      simulateOnChange(component, WHITE_SPACES);
      simulateOnClick(component);
      sinon.assert.notCalled(props.dispatch);
      sinon.assert.notCalled(actions.createNote);
    });
    it('should trim white spaces', function() {
      const testValueWithSpaces = WHITE_SPACES + TEST_VALUE + WHITE_SPACES;
      simulateOnChange(component, testValueWithSpaces);
      simulateOnClick(component);
      sinon.assert.calledOnce(props.dispatch);
      sinon.assert.calledOnce(actions.createNote);
      sinon.assert.calledWithExactly(actions.createNote, TEST_VALUE);
    });
  });
});
