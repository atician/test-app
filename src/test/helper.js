import React from 'react';
import { shallow, mount } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

export const WHITE_SPACES = ' \f\n\r\t\v\u00a0\u2000\u200a\u2028\u2029\u202f\u205f\u3000\ufeff';

export const shallowMount = (Component, props) => {
  return shallow(<Component {...props} />);
};

export const enzymeMount = (Component, props) => {
  return mount(<Component {...props} />);
};

export const simulateOnChange = (component, value = '', element = 'input') =>
  component.find(element).prop('onChange')({ target: { value } });

export const simulateOnClick = (component, element = 'button') =>
  component.find(element).prop('onClick')();
