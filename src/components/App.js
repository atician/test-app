import React from 'react';

import AddNote from '../containers/AddNote';
import NoteList from '../containers/NoteList';
// import './App.css';

const App = () => (
  <div className="App">
    <AddNote/>
    <NoteList/>
  </div>
);

export default App;
