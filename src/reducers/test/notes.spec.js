import { expect } from 'chai';

import notes from '../notes';
import {
  GET_NOTE_SUCCESS,
  POST_NOTE_SUCCESS
} from '../../actions';

describe('notes reducer', function() {
  it('should populate notes on success', function() {
    const action = {
      type: GET_NOTE_SUCCESS,
      notes: [
        {
          id: 1,
          content: 'test'
        }
      ]
    };
    expect(notes([], action)).to.eql(action.notes);
  });
  it('should add new note to list of notes', function() {
    const action = {
      type: POST_NOTE_SUCCESS,
      id: 1,
      content: 'test1'
    };
    const state = [
      {
        id: 0,
        content: 'test0'
      }
    ];
    const newState = [
      {
        id: 0,
        content: 'test0'
      },
      {
        id: 1,
        content: 'test1'
      }
    ];
    expect(notes(state, action)).to.eql(newState);
  });
});
