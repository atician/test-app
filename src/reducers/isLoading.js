import {
  GET_NOTE_REQUEST,
  GET_NOTE_SUCCESS,
  GET_NOTE_FAILURE,
  POST_NOTE_REQUEST,
  POST_NOTE_SUCCESS,
  POST_NOTE_FAILURE
} from '../actions';

const isLoading = (state = false, action) => {
  switch (action.type) {
    case GET_NOTE_REQUEST:
    case POST_NOTE_REQUEST:
      return true;
    case GET_NOTE_SUCCESS:
    case GET_NOTE_FAILURE:
    case POST_NOTE_SUCCESS:
    case POST_NOTE_FAILURE:
      return false;
    default:
      return false;
  }
};

export default isLoading;
